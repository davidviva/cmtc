package com.avaya.CMTCService.ews;

import com.avaya.CMTCService.domain.Meeting;
import com.avaya.CMTCService.entity.OutlookJsonRequest;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.BasePropertySet;
import microsoft.exchange.webservices.data.core.enumeration.property.DefaultExtendedPropertySet;
import microsoft.exchange.webservices.data.core.enumeration.property.MapiPropertyType;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.exception.service.remote.ServiceResponseException;
import microsoft.exchange.webservices.data.core.service.item.Appointment;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.FolderId;
import microsoft.exchange.webservices.data.property.complex.Mailbox;
import microsoft.exchange.webservices.data.property.definition.ExtendedPropertyDefinition;
import microsoft.exchange.webservices.data.search.CalendarView;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.ItemView;
import microsoft.exchange.webservices.data.search.filter.SearchFilter;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by qqiao on 7/15/2015.
 */

@Component
public class ExchangeCheckService {

    final static String SERVICE_ACCOUNT = "cmtcservice";
    final static String PASSWORD = "WiSvrcM15";
    final static String DOMAIN = "@avaya.com";
    final static String EXCHANGE_URL = "https://usmail.avaya.com/ews/Exchange.asmx";
    final static Logger logger = Logger.getLogger(ExchangeCheckService.class);

    String delegatorEmail;
    String appointmentId;
    long oldMeetingTime;
    long newMeetingTime;

/*
    input exchange account name and password, return an ExchangeService instance.
 */
    private ExchangeService getExchangeService(String account, String password) throws Exception {
        ExchangeService service = new ExchangeService(
                ExchangeVersion.Exchange2010_SP1);
        ExchangeCredentials credentials = new WebCredentials(account,
                password);
        service.setCredentials(credentials);
        service.setUrl(new URI(EXCHANGE_URL));
        return service;
    }

    /*
       Access the user's calendar folder in Exchange. Return false if get ServiceResponseException. Return true if no exception.
     */
    public boolean checkDelegate(OutlookJsonRequest jr) throws Exception {
        delegatorEmail = jr.user.handle + DOMAIN;
        ExchangeService service = getExchangeService(SERVICE_ACCOUNT,PASSWORD);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        Date startDate, endDate;
        FindItemsResults<Appointment> findAppointments = null;
        try {
            startDate = dateFormat.parse("2015-07-20 00:00:00");
            endDate = dateFormat.parse("2015-07-21 00:00:00");
            Mailbox mailbox = new Mailbox(delegatorEmail);
            FolderId folderId = new FolderId(WellKnownFolderName.Calendar,mailbox);
            CalendarView calendarView = new CalendarView(startDate,endDate);
            findAppointments = service.findAppointments(folderId,calendarView);
            int c = 1;
            for(Appointment a : findAppointments.getItems()){
                logger.debug( c++ +". sub---"+a.getSubject());
               // logger.debug("Ical UID -----" + a.getICalUid());
            }
        }catch (ServiceResponseException e) {
            logger.info("No delegation, return false : " + e.getMessage());
            return false;
        }catch(Exception e){
            logger.error("check delegate exception: " + e.getMessage());
            e.printStackTrace();
        }
        logger.info("Has delegation, return true");
        return true;

    }


    /*
        compare local meeting with Exchange server for checking time change.
        return null if meeting is cancelled or user reject it.
        otherwise return meeting with time in Exchange.
     */
    public Meeting checkTime(Meeting meeting) throws Exception {
        appointmentId = meeting.getAppointmnetId();
        delegatorEmail = meeting.getEmail();
        ExchangeService service = getExchangeService(SERVICE_ACCOUNT,PASSWORD);

        // for encode appointmentId char array to base64 string.
        int length = appointmentId.length() / 2;
        char[] hexChars = appointmentId.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        String base64String = new String(Base64.encodeBase64(d));
        // find appointment in Exchange by this filter
        SearchFilter filter = new SearchFilter.IsEqualTo(new ExtendedPropertyDefinition(DefaultExtendedPropertySet.Meeting, 0x03, MapiPropertyType.Binary ), base64String);
        Mailbox mailbox = new Mailbox(delegatorEmail);
        FolderId folderId = new FolderId(WellKnownFolderName.Calendar,mailbox);
        ItemView itemView = new ItemView(1);
        itemView.setPropertySet(new PropertySet(BasePropertySet.FirstClassProperties));
        ArrayList<Item> items = service.findItems(folderId, filter, itemView).getItems();

        if(items.size()>0){
            Appointment appointment = (Appointment) items.get(0);
            if(appointment.getIsCancelled()) {
                logger.info("meeting is cancelled");
                return null;
            }
            oldMeetingTime = meeting.getTimeToCall().getTime();
            newMeetingTime = appointment.getStart().getTime();
            if(Math.abs(oldMeetingTime-newMeetingTime)<(1000 * 59)){  // 1 min
                logger.info("meeting is still same");
                return meeting;
            }else{
                Timestamp newTS = new Timestamp(newMeetingTime);
                meeting.setTimeToCall(newTS);
                logger.info("meeting time changed more than 1 mins");
                logger.debug("new start time: " + meeting.getTimeToCall());
                return meeting;
            }
        }else{
            logger.info("can not find this appointment");
            return null;
        }

    }

    private byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }


}
