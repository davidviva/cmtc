package com.avaya.CMTCService.domain;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class MeetingQueue{
    private MyQueue myQueue = new MyQueue();
    private Timestamp queueStartTime;
    private Timestamp queueEndTime;
    
	public MyQueue getMyQueue() {
		return myQueue;
	}

	public void setMyQueue(MyQueue myQueue) {
		this.myQueue = myQueue;
	}
	
	public Timestamp getQueueEndTime(){
		return this.queueEndTime;
	}
	
	public void setQueueEndTime(Timestamp ts){
		this.queueEndTime=ts;
	}
	
	public Timestamp getQueueStartTime(){
		return this.queueStartTime;
	}
	
	public void setQueueStartTime(Timestamp ts){
		this.queueStartTime=ts;
	}

}
