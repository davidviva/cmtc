package com.avaya.CMTCService.domain;

import com.avaya.CMTCService.service.impl.MeetingServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/*
Batch for collecting used meetings, and delete them from DB when batch is full.
 */
@Component
public class OutDateBatch {

	private static final int BATCH_SIZE = 10;
	public ArrayList<Meeting> batch = new ArrayList<Meeting>();
	final static Logger logger = Logger.getLogger(OutDateBatch.class);

	@Autowired
	MeetingServiceImpl meetingService;

	public boolean cleanAndAad(Meeting meeting){
		boolean result = false;

		if(batch.size()<BATCH_SIZE){
			batch.add(meeting);
			logger.debug("add to batch, current batch size: " + batch.size());
		}else{
			meetingService.batchDelete(batch);
			batch.clear();
			batch.add(meeting);
			result=true;
			logger.info("Clear batch and delete from DB.");
		}

		return result;
	}

}
