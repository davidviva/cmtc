package com.avaya.CMTCService.domain;

import javax.persistence.*;
import java.sql.Timestamp;
@Entity
@Table(name="meeting")
public class Meeting {

	/*
	@meetingPK Integer
	Unique identify of a meeting.
	Primary key in the database, auto generated.
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)     // AUTO
	@Column(name="MEETING_SCHED_ID", nullable=false)   // unique=true, nullable=false
	private Integer meetingPK;

	/*
	@remId Integer
	ID for the reminder item.
	hash from handle and appointmentId. Used for identify a record(search, delete action).
	 */
	@Column(name="REM_ID", nullable=false)
	private Integer remId;

	@Column(name="PHONE", nullable= false)
	private String phone;
	
	@Column(name="CONF_CODE")
	private String confCode;

	/*
	@type Integer
	refer to MeetingType class
	 */
	@Column(name="TYPE")
	private Integer type;
	
	@Column(name="RETRY")
	private int retry;
	
	@Column(name="RETRY_WAIT")
	private int retryWait;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="HANDLE")
	private String handle;
	
	@Column(name="DISPLAY_NAME")
	private String displayName;
	
	@Column(name="COUNTRY_CODE")
	private int countryCode;
	
	@Column(name="REGION_CODE")
	private String regionCode;
	
	@Column(name="LOCALE")
	private String locale;
	
	@Column(name="MEETING_TITLE")
	private String meetingTitle;
	
	@Column(name="UTC_OFFSET")
	private String utcOffset;
	
	@Column(name="TIME_CALL")
	private Timestamp timeToCall;

	/*
	@appointmentId String
	send by plug in, same as ICalID in Exchange server
	 */
	@Column(name="APPOINTMENT_ID")
	private String appointmnetId;

	/*
	@delegate boolean
	true - user grand delegation to cmtc service mailbox
	false - user have not grant delegation yet.
	 */
	@Column(name="DELEGATE")
	private boolean delegate;

	public boolean isDelegate() {
		return delegate;
	}

	public void setDelegate(boolean delegate) {
		this.delegate = delegate;
	}

	public String getAppointmnetId() {
		return appointmnetId;
	}

	public void setAppointmnetId(String appointmnetId) {
		this.appointmnetId = appointmnetId;
	}

	public Timestamp getTimeToCall() {
		return timeToCall;
	}

	public void setTimeToCall(Timestamp timeToCall) {
		this.timeToCall = timeToCall;
	}

	public Integer getMeetingPK() {
		return meetingPK;
	}

	public void setMeetingPK(Integer meetingPK) {
		this.meetingPK = meetingPK;
	}

	public Integer getRemId() {
		return remId;
	}

	public void setRemId(Integer remId) {
		this.remId = remId;
	}

	public String getConfCode() {
		return confCode;
	}

	public void setConfCode(String confCode) {
		this.confCode = confCode;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public int getRetry() {
		return retry;
	}

	public void setRetry(int retry) {
		this.retry = retry;
	}

	public int getRetryWait() {
		return retryWait;
	}

	public void setRetryWait(int retryWait) {
		this.retryWait = retryWait;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public int getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getMeetingTitle() {
		return meetingTitle;
	}

	public void setMeetingTitle(String meetingTitle) {
		this.meetingTitle = meetingTitle;
	}

	public String getUtcOffset() {
		return utcOffset;
	}

	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}

	public String getPhone(){
		return phone;
	}
	
	public void setPhone(String phone){
		this.phone=phone;
	}
	
	

}
