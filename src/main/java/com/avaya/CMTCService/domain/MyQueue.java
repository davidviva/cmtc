package com.avaya.CMTCService.domain;

import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MyQueue {
	private static List<Meeting> qList = Collections
			.synchronizedList(new LinkedList<Meeting>());
	final static Logger logger = Logger.getLogger(MyQueue.class);

	/*
		insert or update the meeting, order by time
	 */
	public void insertOrUpdate(Meeting meeting) throws Exception{
		Timestamp ts = meeting.getTimeToCall();
			if (qList.size() == 0) {
				qList.add(meeting);
				logger.debug("first added in queue");
			} else if (qList.get(0).getTimeToCall().after(ts)) {
				qList.add(0, meeting);
				logger.debug("earliest in queue");
			} else if (qList.get(qList.size() - 1).getTimeToCall().before(ts)) {
				qList.add(meeting);
				logger.debug("last one in queue");
			} else {
				for (Meeting mt : qList) {
					if (mt.getTimeToCall().before(ts)) {
						continue;
					} else if (mt.getTimeToCall().equals(ts)) {
						if (mt.getRemId().equals(meeting.getRemId())
								&& mt.getType().equals(meeting.getType())) {
							logger.info("queue update phone number");
							mt.setPhone(meeting.getPhone());
							break;
						} else {
							if (qList.indexOf(mt) == qList.size() - 1) {
								qList.add(meeting);
								logger.debug("create at last");
								break;
							} else {
								continue;
							}
						}
					} else {
						int seat = qList.indexOf(mt);
						qList.add(seat, meeting);
						logger.info("queue create one");
						break;
					}
				}
			}
	}

	/*
	 * delete all same remId when type = 0
	 */
	public int deleteById(Integer remId) {
		int i = 0;
		int counter = 0;
		Timestamp ts = null;
		while (i < qList.size()) {
			if (qList.get(i).getRemId().equals(remId)) {
				Meeting mt = qList.remove(i);
				ts = mt.getTimeToCall();
				counter++;
			} else if (ts != null && ts.before(qList.get(i).getTimeToCall())) {
				break;
			} else {
				i++;
			}
		}
		return counter;
	}

	/*
	 * delete when type!=0
	 */
	public int deleteOne(Integer remId, Integer type) {
		int i = 0;
		int s = qList.size();
		while (i < s) {
			Meeting mt = qList.get(i);
			if (mt.getRemId().equals(remId) && mt.getType().equals(type)) {
				qList.remove(i);
				return 1;
			} else {
				i++;
			}
		}
		return 0;
	}

	public int size() {
		return qList.size();
	}

	public boolean isEmpty() {
		return qList.size() == 0 ? true : false;
	}

	public boolean add(Meeting e) {
		return qList.add(e);
	}

	public Meeting peek() {
		return qList.get(0);
	}

	public Meeting poll() {
		return qList.remove(0);
	}


}
