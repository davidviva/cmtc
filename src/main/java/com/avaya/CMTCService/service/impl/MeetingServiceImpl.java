package com.avaya.CMTCService.service.impl;

import com.avaya.CMTCService.dao.MeetingDao;
import com.avaya.CMTCService.domain.Meeting;
import com.avaya.CMTCService.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;

@Service
public class MeetingServiceImpl implements MeetingService {
    @Autowired
    private MeetingDao meetingDao;


    public Meeting create(Meeting meeting) {
        return meetingDao.create(meeting);
    }

    public Meeting updateOrCreate(Meeting meeting) {
        return meetingDao.updateOrCreate(meeting);
    }

    public int update(Meeting meeting) {
        return meetingDao.update(meeting);
    }

    public int delete(Meeting meeting) {
        return meetingDao.delete(meeting);
    }

    public ArrayList<Meeting> retriveUpcomingMeetings(Timestamp startTimestamp) {
        return meetingDao.retriveUpcomingMeetings(startTimestamp);
    }

    public Meeting find(String remId) {
        return null;
    }

    public ArrayList<Meeting> retriveByTime(Timestamp timeStart, Timestamp timeEnd) {
        return meetingDao.retriveByTime(timeStart, timeEnd);
    }

    public Meeting update(Integer meetingId) {
        return null;
    }

    public void batchDelete(ArrayList<Meeting> meetings) {
        meetingDao.batchDelete(meetings);
    }

}
