package com.avaya.CMTCService.service;

import com.avaya.CMTCService.domain.Meeting;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.ArrayList;

@Component
public interface MeetingService {
	Meeting create(Meeting meeting);
	Meeting updateOrCreate(Meeting meeting);
	int update(Meeting meeting);
	int delete(Meeting meeting);
	ArrayList<Meeting> retriveUpcomingMeetings(Timestamp startTimestamp);
	Meeting find(String meetingId);
	ArrayList<Meeting> retriveByTime(Timestamp timeStart, Timestamp timeEnd);
	void batchDelete(ArrayList<Meeting> meetings);

}
