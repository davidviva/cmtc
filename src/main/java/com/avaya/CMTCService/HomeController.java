package com.avaya.CMTCService;


import com.avaya.CMTCService.domain.Meeting;
import com.avaya.CMTCService.domain.MeetingQueue;
import com.avaya.CMTCService.domain.OutDateBatch;
import com.avaya.CMTCService.entity.OutlookJsonRequest;
import com.avaya.CMTCService.entity.OutlookJsonResponse;
import com.avaya.CMTCService.ews.ExchangeCheckService;
import com.avaya.CMTCService.service.MeetingService;
import com.avaya.CMTCService.util.CallByTypeService;
import com.avaya.CMTCService.util.JasksonService;
import com.avaya.CMTCService.util.MeetingGenerator;
import com.avaya.CMTCService.util.MeetingType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;

@RestController
public class HomeController {
	@Autowired
	private MeetingService meetingService;
	@Autowired
	private MeetingQueue meetingQueue;
	@Autowired
	private MeetingGenerator meetingGenerator;
	@Autowired
	private CallByTypeService callService;
	@Autowired
	private OutDateBatch batchService;
	@Autowired
	private ExchangeCheckService exchangeCheckService;

	final static Logger logger = Logger.getLogger(HomeController.class);


	/*
	  For receiving meeting 'call me to join' request from outlook.
	 */
	@RequestMapping(value = "meeting/schedule", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public String getOutlookMessage(@RequestBody OutlookJsonRequest jr) {

		logger.info(JasksonService.toJsonString(jr));
		OutlookJsonResponse response = new OutlookJsonResponse();

		if(jr.user.countryCode==null || jr.user.countryCode.equals("") ){
			response.updateStatus(2);
		}else if(jr.user.phone==null || jr.user.phone.equals("")){
			response.updateStatus(2);
		}else if(jr.confCode==null || jr.confCode.equals("")){
			response.updateStatus(2);
		}else if(jr.time==null || jr.time.equals("")){
			response.updateStatus(2);
		}else if(jr.type==0){
			response.updateStatus(2);
		}else
		 {   boolean delegate = false;
			 try {
				 delegate = exchangeCheckService.checkDelegate(jr);
			 }catch(Exception e){
				 logger.error("check delegate exception: " + e.getMessage());
				 e.printStackTrace();
			 }
			Meeting meeting = meetingGenerator.generate(jr,delegate);
			Timestamp ts = meeting.getTimeToCall();
		/*
		 * check the time, if less than 5 min with current time, put into queue;
		 * if less than 12 s , make call
		 * if after 5 min, save it into DB
		 */
			long d = new Date().getTime();
			long duration = (long) (1000 * 12);
			Timestamp shortTS = new Timestamp(d + duration);
			Timestamp longTS = meetingQueue.getQueueEndTime();
			if (ts.before(shortTS)) {
				logger.debug("in 12 s");
				callService.makeCallNow(meeting);
			} else if (ts.before(longTS)) {
				logger.debug("in 5 min");
				synchronized (meetingQueue.getMyQueue()) {
					try {
						meetingQueue.getMyQueue().insertOrUpdate(meeting);
					}catch(Exception e){
						logger.error("meeting queue insertOrUpdate exception: " + e.getMessage());
						response.updateStatus(2);
						e.printStackTrace();
					}
				}
			} else {
				logger.debug("save to DB");
				meetingService.updateOrCreate(meeting);
			}
			 response.updateStatus(1);
		}

		logger.info("response to outlook: " + response.getStatus());
		return JasksonService.toJsonString(response);

	}

	/*
	  For receiving cancel 'call me to join' request from Outlook
	 */
	@RequestMapping(value = "meeting/cancel", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public String deleteMeeting(@RequestBody OutlookJsonRequest jr) {

		logger.info("cancel request : "
				+ JasksonService.toJsonString(jr));
		OutlookJsonResponse response = new OutlookJsonResponse();

			try {
				boolean delegate = exchangeCheckService.checkDelegate(jr);
				Meeting meeting = meetingGenerator.generate(jr,delegate);
				int type = meeting.getType();
			    int remId = meeting.getRemId();
				Timestamp ts = meeting.getTimeToCall();
				Timestamp longTS = meetingQueue.getQueueEndTime();

				/*
				  check if meeting time before queue end time, delete from queue.
				  else, delete from DB.
				 */
				if (ts.before(longTS)) {
					logger.info("before queue end time");
					if (type == MeetingType.RESERVE_ACTION) {
						synchronized (meetingQueue.getMyQueue()) {
							int result = meetingQueue.getMyQueue().deleteById(remId);
							logger.info("delete from queue type 0, result: "
								+ result);
							batchService.cleanAndAad(meeting);
						}
					} else {
						synchronized (meetingQueue.getMyQueue()) {
							int result = meetingQueue.getMyQueue().deleteOne(remId, type);
							logger.info("delete from queue type 1234, result: "
								+ result);
						}
					}
				} else {
					int result = meetingService.delete(meeting);
					logger.info("delete from DB, result: " + result);
			}
		} catch (Exception e) {
			logger.error("cancel exception : " + e.getMessage());
			e.printStackTrace();
			response.updateStatus(2);
			logger.info("response to outlook: " + response.getStatus());
			return JasksonService.toJsonString(response);
		}

		response.updateStatus(1);
		logger.info("response to outlook: " + response.getStatus());
		return JasksonService.toJsonString(response);
	}





}
