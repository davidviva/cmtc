package com.avaya.CMTCService.entity;

/*
JSON request from Outlook plug-in for scheduling/canceling meetings.
 */
public final  class OutlookJsonRequest {
	
	public User user;
	/*
	refer to MeetingType
	 */
	public int type;
	/*
	how many times retry before user joins meeting
	 */
	public int retry;
	/*
	seconds to wait between consecutive calls
	 */
	public int retryWait;
	/*
	hash from handle+appointmentId
	 */
	public int remId;
	public String confCode;
	public String time;
	public String meetingTitle;
	/*
	unique appointment ID on Exchange
	 */
	public String appointmentId;

	public final class User{
		
		public String email;
		public String handle;
		public String displayName;
		/*
		 phone number without country code
		 */
		public String phone;
		public String regionCode;
		public String countryCode;
		/*
		Time zone information
		 */
		public String locale;
		/*
		Duplicate info with locale for different platforms
		 */
		public String utcOffset;
		
	}
	
	
	/*@Override
	public String toString(){
		String js="";
		js += "{";
		js += "\"user\":";  
		js += "{";
		js += "\"email\": \""+ this.user.email.trim() +"\",";
		js += "\"handle\": \""+ this.user.handle.trim() +"\",";
		js += "\"displayName\": \""+ this.user.displayName +"\",";
		js += "\"phone\": \""+ this.user.phone.trim() +"\",";
		js += "\"regionCode\": \""+ this.user.regionCode +"\",";
		js += "\"countryCode\": \""+ this.user.countryCode +"\",";
		js += "\"locale\": \""+ this.user.locale +"\",";
		js += "\"utcOffset\": \""+ this.user.utcOffset +"\"";
		js += "},";
				
		js += "\"type\": \""+ this.type +"\",";
		js += "\"retry\": \""+ this.retry +"\",";
		js += "\"retryWait\": \""+ this.retryWait +"\",";
		js += "\"remId\": \""+ this.remId +"\",";
		js += "\"confCode\": \""+ this.confCode.trim() +"\",";
		js += "\"time\": \""+ this.time.trim() +"\",";
		js += "\"meetingTitle\": \""+ this.meetingTitle.trim() +"\"";
		js += "}";
		
		return js;
	}*/
	
	
}
