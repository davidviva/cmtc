package com.avaya.CMTCService.entity;

/*
JSON response to Outlook plug-in
 */
public class OutlookJsonResponse {

//	public String status;
	private Boolean status;

	public OutlookJsonResponse() {
		this.status=false;
	}

	public void updateStatus(int response) {
		switch (response) {
		case 1:
			this.status = true;
			break;
		case 2:
			this.status = false;
			break;
		default:
			this.status = false;
			break;
		}
	}

	public Boolean getStatus(){
		return this.status;
	}


}
