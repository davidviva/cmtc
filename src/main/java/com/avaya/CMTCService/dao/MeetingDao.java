package com.avaya.CMTCService.dao;

import com.avaya.CMTCService.domain.Meeting;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;

@Component
@Transactional
public interface MeetingDao {
	Meeting create(Meeting meeting);
	Meeting updateOrCreate(Meeting meeting);

	int update(Meeting meeting);

	int delete(Meeting meeting);

	ArrayList<Meeting> retriveUpcomingMeetings(Timestamp startTimestamp);
	
	ArrayList<Meeting> retriveByTime(Timestamp timeStart, Timestamp timeEnd);
	
	void batchDelete(ArrayList<Meeting> meetings);

}
