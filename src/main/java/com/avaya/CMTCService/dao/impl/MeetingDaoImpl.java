package com.avaya.CMTCService.dao.impl;

import com.avaya.CMTCService.dao.MeetingDao;
import com.avaya.CMTCService.domain.Meeting;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;

@Repository
@Transactional
public class MeetingDaoImpl implements MeetingDao {
	@Autowired
	@Qualifier("CMTCSessionFactory")
	public SessionFactory sessionFactory;
	final static Logger logger = Logger.getLogger(MeetingDaoImpl.class);
	private String hql = "from Meeting where time_call between ? and ? order by time_call";
	private String getDelegationMeetingsHQL ="from Meeting where time_call > ? and delegate = true order by time_call" ;
	private String updatePhoneHQL = "update Meeting set phone = :phone, time_call = :time_call where rem_id= :rem_id and type= :type";
	private String deleteOneHQL = "delete Meeting where rem_id = :rem_id and type = :type";
	private String deleteAllHQL = "delete Meeting where rem_id = :rem_id";


	public Meeting create(Meeting meeting) {
		sessionFactory.getCurrentSession().save(meeting);
		sessionFactory.getCurrentSession().flush();
		return meeting;
	}

	/*
	 * requirement: when type==type && remId==remId, update phone else create
	 * one
	 */
	public Meeting updateOrCreate(Meeting meeting) {
		int result = this.update(meeting);
		if (result > 0) {
			logger.debug("has same, update");
			return meeting;
		} else {
			logger.debug("create one");
			return this.create(meeting);
		}
	}

	public int update(Meeting meeting) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				updatePhoneHQL);
		query.setParameter("rem_id", meeting.getRemId());
		query.setParameter("time_call", meeting.getTimeToCall());
		query.setParameter("type",meeting.getType());
		query.setParameter("phone", meeting.getPhone());
		int result = query.executeUpdate();
		return result;
	}

	public int delete(Meeting meeting) {
		int type = meeting.getType();
		int remId = meeting.getRemId();
		int result;
		if (type == 0) {
			Session ss = sessionFactory.openSession();
			Query query = ss.createQuery(
					deleteAllHQL);
			query.setParameter("rem_id", remId);
			logger.debug("delete by rem_id: " + remId);
			result = query.executeUpdate();
			ss.flush();
			ss.close();
			logger.info("delete meeting type=0: " + result);
		} else {
			Query query = sessionFactory.getCurrentSession().createQuery(
					deleteOneHQL);
			query.setParameter("rem_id", remId);
			query.setParameter("type", type);
			result = query.executeUpdate();
		}
		logger.info("delete resulte: " + result);
		sessionFactory.getCurrentSession().flush();
		return result;
	}

	public void batchDelete(final ArrayList<Meeting> meetings) {

		Session ss = sessionFactory.openSession();
		for (Meeting meet : meetings) {
			Query query = ss.createQuery(deleteOneHQL);
			query.setParameter("rem_id", meet.getRemId());
			query.setParameter("type", meet.getType());
			int i = query.executeUpdate();
			ss.flush();
			logger.info("delete result: " + i);
		}
		ss.clear();
		ss.close();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Meeting> retriveByTime(Timestamp timeStart,
			Timestamp timeEnd) {
		Session session = sessionFactory.openSession();
		ArrayList<Meeting> meetings = (ArrayList<Meeting>) session
				.createQuery(hql).setParameter(0, timeStart)
				.setParameter(1, timeEnd).list();
		session.close();
		return meetings;
	}

	public ArrayList<Meeting> retriveUpcomingMeetings(Timestamp startTimestamp) {
		Session session = sessionFactory.openSession();
		ArrayList<Meeting> meetings = (ArrayList<Meeting>) session
				.createQuery(getDelegationMeetingsHQL).setParameter(0, startTimestamp).list();
		session.close();
		return meetings;

	}

}
