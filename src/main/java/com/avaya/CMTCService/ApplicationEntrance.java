package com.avaya.CMTCService;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by Yuwen on 4/9/2015.
 */
@SpringBootApplication
@EnableScheduling
public class ApplicationEntrance {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(ApplicationEntrance.class, args);
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        String log4jConfPath = "application.properties";
        PropertyConfigurator.configure(classLoader.getResource(log4jConfPath));
    }
}