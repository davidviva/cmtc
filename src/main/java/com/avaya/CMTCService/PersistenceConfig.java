package com.avaya.CMTCService;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * Created by ydai on 4/9/2015.
 */
@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@PropertySource("application.properties")
public class PersistenceConfig {
    @Autowired
    private DataSource pooledDataSource;
    @Value("${hibernate.showSql}")
    private String showSql;
    @Value("${hibernate.formatSql}")
    private String formatSql;
    @Value("${hibernate.useSqlComments}")
    private String useSqlComments;
    @Value("${hibernate.dialect}")
    private String hibernateDialect;
    private SessionFactory sessionFactory;
    @Bean(name="CMTCSessionFactory")
    public SessionFactory createMysqlSessionFactory () {
        LocalSessionFactoryBuilder sessionFactoryBuilder = new LocalSessionFactoryBuilder(pooledDataSource);
        sessionFactoryBuilder.setProperty("hibernate.show_sql", showSql);
        sessionFactoryBuilder.setProperty("hibernate.format_sql", formatSql);
        sessionFactoryBuilder.setProperty("hibernate.use_sql_comments", useSqlComments);
        sessionFactoryBuilder.setProperty("hibernate.dialect", hibernateDialect);
        // scan all entity classes
        sessionFactoryBuilder.scanPackages("com.avaya.CMTCService");
        sessionFactory = sessionFactoryBuilder.buildSessionFactory();
        return sessionFactory;
    }

    @Bean
    public Session createHibernateSession (SessionFactory sessionFactory) {
        //return sessionFactory.getCurrentSession();
        return sessionFactory.openSession();
    }

    @Bean
    public PlatformTransactionManager transactionManager(SessionFactory sessionFactory) {

        return new HibernateTransactionManager(sessionFactory);
    }

    /**
     * for edp datasource
     * @return
     */
  /*  @Bean(name="EDPSessionFactory")
    public SessionFactory createEDPMysqlSessionFactory(){
        LocalSessionFactoryBuilder sessionFactoryBuilder = new LocalSessionFactoryBuilder(pooledDataSource);
        sessionFactoryBuilder.setProperty("hibernate.show_sql", showSql);
        sessionFactoryBuilder.setProperty("hibernate.format_sql", formatSql);
        sessionFactoryBuilder.setProperty("hibernate.use_sql_comments", useSqlComments);
        sessionFactoryBuilder.setProperty("hibernate.dialect", hibernateDialect);
        // scan all entity classes
        sessionFactoryBuilder.scanPackages("com.avaya.CMTCService");
        sessionFactory = sessionFactoryBuilder.buildSessionFactory();
        return sessionFactory;

    }*/

    /*@Bean
    public Session createEDPHibernateSession (SessionFactory sessionFactory) {
        //return sessionFactory.getCurrentSession();
        return sessionFactory.openSession();
    }*/

}