package com.avaya.CMTCService.util;

/**
 * Created by qqiao on 4/9/2015.
 */
public class BridgeService {

    public static String getBridgeNumber(int countryCode){
        // Pre-process countryCode

        switch (countryCode){
            case 1:return "+15132288888"; // Optional +1-866-282-9228  +1-613-595-9444
            // +1-720-977-2777	US Secondary Number  +1-613-771-7550	Canada (Belleville) Secondary Number
            case 7: return "+74995004556"; // Russia number
            case 27: return "+27117004682"; // South Africa
            case 31: return "+31306097888"; // Netherland
            case 32: return "+3227777778"; // Belgium Main Number
            case 33: return "+33140947902"; // France Main Number
            case 34: return "+34913876888"; // Spain Main Number
            case 36: return "+3612388400"; // Hungary Main Number
            case 39: return "+390226293208"; // Italy Main Number
            case 41: return "+41448781501"; // Switzerland Main Number
            case 43: return "+431878706288"; // Austria Main Number
            case 44: return "+441483308888"; // UK Main Number
            case 45: return "+4544737318"; // Denmark Main Number
            case 46: return "+46852207018"; // Sweden Main Number
            case 47: return "+4722937848"; // Norway Main Number
            case 48: return "+48225773805"; // Poland Main Number
            case 49: return "+496975058888"; // Germany Main Number
            case 52: return "+525552782850"; // Mexico Main Number
            case 54: return "+541141198299"; // Argentina Main Number
            case 55: return "+551151856372"; // Brazil Main Number
            case 60: return "+60320593399"; // Malaysia Main Number
            case 61: return "+61293529299"; // Australia Main Number
            case 62: return "+622125559859"; // Indonesia Main Number
            case 63: return "+6328305991"; // Philippines Main Number
            case 65: return "+6564038988"; // Singapore Main Number
            case 66: return "+6626904808"; // Thailand Main Number
            case 81: return "+81355758799"; // Japan Main Number
            case 82: return "+82260074669"; // South Korea Main Number
            case 85: return "+85231216299"; // Hong Kong Main Number
            case 86: return "+861085165799"; // Beijing, CN Main Number
		/*+86-20-22647099	Guangzhou, CN Main Number
		+86-21-61206196	Shanghai, CN Main Number
		+86-411-83777888	Dalian, CN Main Number
		+86-760-23614099	Zhongshan, CN Main Number*/
            case 90: return "+902129999087"; // Turkey Main Number
            case 91: return "+912233854100"; // Mumbai, IN Main Number
		/*+91-20-67247009	Pune, IN Main Number
		+91-40-44317006	Hyderabad, IN Main Number
		+91-80-67153211	Bangalore, IN Main Number
		+91-124-4914948	Gurgaon, IN (GB Park) Main Number
		+91-124-4997947	Gurgaon, IN (Vipul Plaza) Main Number*/
            case 353: return "+35391733988"; // Ireland Main Number
            case 385: return "+38515555419"; // Croatia Main Number
            case 420: return "+420222194188"; // Czech Republic Main Number
            case 886: return "+886281713629"; // Taiwan Main Number
            case 966: return "+96612738188"; // Saudi Arabia Main Number
            case 971: return "+97144048288"; // United Arab Emirates Main Number
            case 972: return "+97236459178"; // Israel Main Number

            default: return "+15132288888";
        }
    }


}
