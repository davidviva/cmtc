package com.avaya.CMTCService.util;

import com.avaya.CMTCService.domain.Meeting;
import com.avaya.CMTCService.entity.MakeCallJsonRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

@Component
public class EDPMakeCallService {
	
	private static String HOSTNAME = "135.9.39.61";
	private static String EDP_MAKE_CALL_URL="https://"+HOSTNAME+"/services/AddParticipantToConf/ws/addParticipantToConf/";

	final static Logger logger = Logger.getLogger(EDPMakeCallService.class);

	public void makeCall(Meeting meeting) {
		int type = meeting.getType();
		int countryCode = meeting.getCountryCode();
		MakeCallJsonRequest callRequest = new MakeCallJsonRequest(meeting.getConfCode(), meeting.getDisplayName(),countryCode,meeting.getPhone());
		if(type==MeetingType.SCOPIA_CALL||type==MeetingType.SCOPIA_ALL){
			callRequest.setConfSys("Scopia");
		}else{
			callRequest.setConfSys("AAC");
			callRequest.setConfBridge(countryCode);
		}

		String jsonString = JasksonService.toJsonString(callRequest);
		logger.debug("callRequest json: " + jsonString);

		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				if (hostname.equals(HOSTNAME))
					return true;
				return false;
			}
		});

		HttpURLConnection conn = null;
		try {
			URL url = new URL(EDP_MAKE_CALL_URL);

			SSLContext ssl = SSLContext.getInstance("TLSv1");
			ssl.init(null, new TrustManager[] { new SimpleX509TrustManager() },
					null);
			SSLSocketFactory factory = ssl.getSocketFactory();

			conn = (HttpURLConnection) url.openConnection();
			((HttpsURLConnection) conn).setSSLSocketFactory(factory);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.connect();

			// Send request
			OutputStreamWriter out = new OutputStreamWriter(
					conn.getOutputStream());
			out.write(jsonString);
			out.close();
			logger.debug("addParticipantToConf server response code:"
					+ conn.getResponseCode());
			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED
					&& conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode() + " Message: "
						+ conn.getResponseMessage());
			}

			//Get Response
			InputStream is = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while((line = br.readLine()) != null) {
				response.append(line);
				response.append('\r');

			}
			br.close();
			logger.info("postResponse: Message: " + response.toString());

		} catch (MalformedURLException e) {
			logger.error("Malformed URL Exception: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("IO Exception: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error(" Exception: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (conn != null) {				
				conn.disconnect();
				logger.debug("disconnect");
			}
		}
	}

	/*
	 * for ssl certificate
	 */

	public class SimpleX509TrustManager implements X509TrustManager {

		public void checkClientTrusted(X509Certificate[] arg0, String arg1)
				throws CertificateException {
		}

		public void checkServerTrusted(X509Certificate[] arg0, String arg1)
				throws CertificateException {
		}

		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}

	}

}
