package com.avaya.CMTCService.util;

import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class UTCTimeService {
	final static Logger logger = Logger.getLogger(UTCTimeService.class);

	public static Timestamp stringToTimestamp(String timeCall){
		
		logger.debug("input time string: " + timeCall);
		long l = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
		Date da;
		try {
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			da = sdf.parse(timeCall);
			l=da.getTime();
		} catch (ParseException e) {
			logger.error("time exception: " + e.getMessage());
			e.printStackTrace();
		}		
		Timestamp ts = new Timestamp(l);
		return ts;
	}
	
	public static String timestampToString(Timestamp ts){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));		
		String timeStamp = sdf.format(ts);
		return timeStamp;
		
	}

}
