package com.avaya.CMTCService.util;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.IOException;


public class JasksonService {
	final static Logger logger = Logger.getLogger(JasksonService.class);

	public static String toJsonString(Object o){
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = "";
		 try {
			 jsonString = mapper.writeValueAsString(o);
		} catch (JsonGenerationException e) {
			 logger.error(e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			 logger.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			 logger.error(e.getMessage());
			e.printStackTrace();
		}
		 return jsonString;
	}
	
	 

}
