package com.avaya.CMTCService.util;

import com.avaya.CMTCService.domain.Meeting;
import com.avaya.CMTCService.domain.MeetingQueue;
import com.avaya.CMTCService.domain.OutDateBatch;
import com.avaya.CMTCService.ews.ExchangeCheckService;
import com.avaya.CMTCService.service.MeetingService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;


@Component
public class DemoServiceBasicUsageFixedDelay {

    @Autowired
    private MeetingService meetingService;

    @Autowired
    private MeetingQueue meetingQueue;

    @Autowired
    private CallByTypeService callService;

    @Autowired
    private OutDateBatch batchService;

    @Autowired
    private ExchangeCheckService exchangeCheckService;

    final static Logger logger = Logger.getLogger(DemoServiceBasicUsageFixedDelay.class);

    Timestamp meetingQueueStartTime = null;
    Timestamp meetingQueueEndTime = null;


    /*
     * Timer for query database every 5 mins (1000*60*5)
     * Search for meetings in the next 5 mins
     */
    @Scheduled(fixedDelay = 1000 * 60 * 5)
    public void demoServiceMethod() {

        java.util.Date date = new java.util.Date();
        Timestamp currentTime = new Timestamp(date.getTime());
        meetingQueueStartTime = currentTime;
        Long duration = (long) (1000 * 60 * 5); // 5min
        Timestamp endTime = new Timestamp(date.getTime());
        endTime.setTime(endTime.getTime() + duration);
        meetingQueueEndTime = endTime;

        synchronized (meetingQueue.getMyQueue()) {
            meetingQueue.setQueueStartTime(currentTime);
            meetingQueue.setQueueEndTime(endTime); //
            try {
                ArrayList<Meeting> meetings = meetingService.retriveByTime(
                        currentTime, endTime);
                logger.info("MeetingQueue CurrentTime: " + currentTime);
                logger.info("MeetingQueue EndTime: " + endTime);
                if (meetings != null && meetings.size() != 0) {
                    logger.debug("Has results");
                    logger.info("Retrieved meetings number: "
                            + meetings.size());
                    try {
                        for (Meeting meet : meetings) {
                            Timestamp time = meet.getTimeToCall();
                            logger.debug("timestamp : " + time);
                            meetingQueue.getMyQueue().add(meet);
                        }
                    } catch (Exception e) {
                        logger.error("Add meeting to queue exception: "
                                + e.getMessage());
                        e.printStackTrace();
                    }
                } else {
                    logger.debug("No results found");
                }
            } catch (Exception e) {
                logger.error("Retrieve meetings exception: "
                        + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /*
     * Timer for deQueue each 10 s
     * Get the peek in queue and check if is in the next 10 seconds.
     */
    @Scheduled(fixedDelay = 1000 * 10)
    public void dequeueTimer() {
        long d = new Date().getTime();
        long duration = (long) (1000 * 10); // 10 s
        Date targetDate = new Date(d + duration);

        synchronized (meetingQueue.getMyQueue()) {
            while (meetingQueue.getMyQueue().size() > 0 && meetingQueue.getMyQueue().peek().getTimeToCall().before(targetDate)) {
                Meeting meeting = (Meeting) meetingQueue.getMyQueue().poll();
                logger.debug("queue meeting time : " + meeting.getTimeToCall());
                Timestamp oldMeetingTS = meeting.getTimeToCall();
                /*
                  If the meeting has delegation, check with Exchange again before make call.
                 */
                if(meeting.isDelegate()){
                    logger.debug("before call, check time");
                    try {
                        Meeting newMeet = exchangeCheckService.checkTime(meeting);
                        // if meeting is cancelled, add it to delete batch
                        if(newMeet==null){
                            batchService.cleanAndAad(meeting);
                            logger.debug("check before call : meeting cancelled");
                            continue;
                        }else if(newMeet.getTimeToCall().equals(oldMeetingTS)){
                            logger.debug("meeting does not change");
                            callService.makeCallNow(meeting);
                            batchService.cleanAndAad(meeting);
                        }else if(newMeet.getTimeToCall().after(meetingQueueEndTime)){
                            logger.debug("check before call: meeting postponed");
                            meetingService.updateOrCreate(newMeet);
                        }else{
                            logger.debug("check before call: meeting time change inside this 5 mins");
                            meetingQueue.getMyQueue().add(newMeet);
                        }
                    } catch (Exception e) {
                        logger.error("dequeue timer: Exchange service Retrieve meetings exception: "
                                + e.getMessage());
                        e.printStackTrace();
                        continue;
                    }
                }else{
                    logger.debug("no delegate meeting");
                    callService.makeCallNow(meeting);
                    batchService.cleanAndAad(meeting);
                }
            }
        }
    }


    /*
      Timer for query the database every 7 minutes.
      search for all the upcoming meetings with delegate == true
     */
    @Scheduled(initialDelay=1000*3, fixedDelay = 1000 * 60 * 7)
    public void checkTimeTimer(){

        java.util.Date date = new java.util.Date();
        Timestamp startTime = new Timestamp(date.getTime());
            try {
                ArrayList<Meeting> meetings = meetingService.retriveUpcomingMeetings(startTime);
                logger.info("TimeCheck: StartTime: " + startTime);
                if (meetings != null && meetings.size() != 0) {
                    logger.info("TimeCheck: Retrieved meetings number: "
                            + meetings.size());

                    Timestamp oldMeetTS = null;
                    Meeting newMeet = null;
                    Timestamp newMeetTS = null;
                    Timestamp queueEndTime = meetingQueue.getQueueEndTime();
                    for (Meeting meet : meetings) {
                        oldMeetTS = meet.getTimeToCall();
                        newMeet = exchangeCheckService.checkTime(meet);
                        if(newMeet!=null){
                            newMeetTS = newMeet.getTimeToCall();
                            if(!newMeetTS.equals(oldMeetTS)){
                                if(oldMeetTS.after(queueEndTime)){
                                    if(newMeetTS.after(queueEndTime)){
                                        meetingService.update(newMeet);
                                        logger.info("meeting time changed inside the DB");
                                    }else{
                                        synchronized (meetingQueue.getMyQueue()) {
                                            meetingQueue.getMyQueue().insertOrUpdate(newMeet);
                                        }
                                        meetingService.update(newMeet);
                                        logger.info("meeting time move forward to the queue");
                                    }
                                }else{
                                    if(newMeetTS.after(queueEndTime)){
                                        synchronized (meetingQueue.getMyQueue()) {
                                            int result = meetingQueue.getMyQueue().deleteOne(newMeet.getRemId(), newMeet.getType());
                                        }
                                        meetingService.update(newMeet);
                                        logger.info("meeting postponed, remove from the queue and update the DB. ");
                                    }else{
                                        // time change inside the current queue. delete the old meeting by remId and Type, then insert the new meeting.
                                        meetingQueue.getMyQueue().deleteOne(newMeet.getRemId(), newMeet.getType());
                                        meetingQueue.getMyQueue().add(newMeet);

                                        logger.info("time changes inside the 5 mins queue, queue delete old one and insert new one.");
                                    }
                                }
                            }
                        }else{
                            /*
                            meeting is cancelled or rejected by user
                            delete it from queue/DB
                             */
                            if(oldMeetTS.after(queueEndTime)){
                                meetingService.delete(meet);
                                batchService.cleanAndAad(meet);
                                logger.info("meeting cancelled, delete from the DB");
                            }else{
                                synchronized (meetingQueue.getMyQueue()) {
                                    meetingQueue.getMyQueue().deleteOne(meet.getRemId(), meet.getType());
                                }
                                batchService.cleanAndAad(meet);
                                logger.info("meeting cancelled, delete from the queue");
                            }
                        }
                    }
                } else {
                    logger.debug("TimeCheck: No upcoming meetings");
                }
            } catch (Exception e) {
                logger.error("TimeCheck: Retrieve meetings exception: "
                        + e.getMessage());
                e.printStackTrace();
            }
    }

}
