package com.avaya.CMTCService.util;

import com.avaya.CMTCService.domain.Meeting;
import com.avaya.CMTCService.entity.OutlookJsonRequest;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class MeetingGenerator {
	
	public Meeting generate(OutlookJsonRequest jr, boolean delegate){
		String meetingTitle = jr.meetingTitle;
		String handle = jr.user.handle;
		String timeCall = jr.time;
		Timestamp ts = UTCTimeService.stringToTimestamp(timeCall);
		String appointmentId = jr.appointmentId;
		String confCode = jr.confCode;
		//String hashString = meetingTitle + handle + timeCall + confCode;
		String hashString = handle + appointmentId;
		int hashInt = hashString.hashCode();
		Meeting meeting = new Meeting();
		meeting.setEmail(jr.user.email);
		meeting.setHandle(handle);
		meeting.setDisplayName(jr.user.displayName);
		meeting.setPhone(jr.user.phone);
		meeting.setConfCode(confCode);
		meeting.setType(jr.type);
		meeting.setTimeToCall(ts);
		meeting.setMeetingTitle(meetingTitle);
		meeting.setRemId(hashInt);
		meeting.setCountryCode(Integer.valueOf(jr.user.countryCode));
		meeting.setRegionCode(jr.user.regionCode);
		meeting.setLocale(jr.user.locale);
		meeting.setRetry(jr.retry);
		meeting.setRetryWait(jr.retryWait);
		meeting.setUtcOffset(jr.user.utcOffset);
		meeting.setAppointmnetId(appointmentId);
		meeting.setDelegate(delegate);
		
		return meeting;
	}
}
