package com.avaya.CMTCService.util;

public class MeetingType {
	public static final int RESERVE_ACTION=0;
	public static final int SCOPIA_CALL = 1; 
	public static final int SCOPIA_VIDEO = 2;
	public static final int AAC_CALL = 3;  
	public static final int AAC_VIDEO = 4;
	public static final int SCOPIA_ALL =5;
	public static final int AAC_ALL=6;
}
