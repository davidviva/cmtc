package com.avaya.CMTCService.util;

import com.avaya.CMTCService.domain.Meeting;
import com.avaya.CMTCService.entity.ACJsonRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.net.ssl.*;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;

/*
 Connect to Snap Engage Server for making calls.
 */
@Component
public class AvayaClientMakeCallService {
	private static String HOSTNAME = "135.55.22.67";  //DISI Linux Server
	//private static String HOSTNAME="135.55.32.234";  //YDAI lapton
	private static String AC_MAKE_CALL_URL="https://"+HOSTNAME+":1344/edp";
	final static Logger logger = Logger.getLogger(AvayaClientMakeCallService.class);

	public String acMakeCall(Meeting meeting){

		ACJsonRequest jr  = new ACJsonRequest();
		jr.setConfCode(meeting.getConfCode());
		jr.setHandle(meeting.getHandle());
		jr.setMeetingTitle(meeting.getMeetingTitle());
		jr.setRegionCode(meeting.getRegionCode());
		Timestamp ts = meeting.getTimeToCall();
		String timeStamp = UTCTimeService.timestampToString(ts);
		jr.setTime(timeStamp);
		int type = meeting.getType();
		if(type==MeetingType.AAC_ALL){
			jr.setType(MeetingType.AAC_VIDEO);
		}else if(type==MeetingType.SCOPIA_ALL){
			jr.setType(MeetingType.SCOPIA_VIDEO);
		}else{
			jr.setType(type);
		}
		jr.setUtcOffset(meeting.getUtcOffset());
				
		String jsonString=JasksonService.toJsonString(jr);
		logger.debug("JSON request send to Snap E: " + jsonString);

		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				if (hostname.equals(HOSTNAME))
					return true;
				return false;
			}
		});
				
		HttpURLConnection conn = null;
		try {
			URL url = new URL(AC_MAKE_CALL_URL);
			SSLContext ssl = SSLContext.getInstance("TLSv1");
			ssl.init(
					null,
					new TrustManager[] { new SimpleX509TrustManager() },
					null);
			SSLSocketFactory factory = ssl.getSocketFactory();

			conn = (HttpURLConnection) url.openConnection();
			((HttpsURLConnection) conn).setSSLSocketFactory(factory);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.connect();

			// Send request
			OutputStreamWriter out = new OutputStreamWriter(
					conn.getOutputStream());
			out.write(jsonString);
			out.close();
			logger.debug("Snap Engage server response code:"
					+ conn.getResponseCode());
			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED
					&& conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException(
						"Failed : HTTP error code : "
								+ conn.getResponseCode() + " Message: "
								+ conn.getResponseMessage());
			}

		} catch (MalformedURLException e) {
			logger.error("Malformed URL Exception: "
					+ e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("IO Exception: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error(" Exception: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (conn != null) {
				logger.debug("disconnect");
				conn.disconnect();
			}
		}		
		return "";
	}
	
	/*
	 * for ssl certificate
	 */

public class SimpleX509TrustManager implements X509TrustManager {

		public void checkClientTrusted(X509Certificate[] arg0, String arg1)
				throws CertificateException {
		}

		public void checkServerTrusted(X509Certificate[] arg0, String arg1)
				throws CertificateException {
		}

		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}

	}
			

}
