package com.avaya.CMTCService.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avaya.CMTCService.domain.Meeting;
@Service
public class CallByTypeService {
	@Autowired
	private EDPMakeCallService edpService;
	
	@Autowired
	private AvayaClientMakeCallService acService;
	

	public void makeCallNow(Meeting meeting) {

		int type = meeting.getType();

		switch (type) {
		case MeetingType.RESERVE_ACTION:
			// 0 not be used in schedule call
			break;
		case MeetingType.SCOPIA_CALL:
		case MeetingType.AAC_CALL:
			edpService.makeCall(meeting);
			break;
		case MeetingType.SCOPIA_VIDEO:
		case MeetingType.AAC_VIDEO:
			acService.acMakeCall(meeting);
			break;
		case MeetingType.SCOPIA_ALL:
		case MeetingType.AAC_ALL:
			edpService.makeCall(meeting);
			acService.acMakeCall(meeting);
			break;
		default:
			break;
		}

	}

}
